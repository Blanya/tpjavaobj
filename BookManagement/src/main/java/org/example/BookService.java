package org.example;

import java.util.List;

public interface BookService {

//    Peut être utiliser pour initialisé id etc
//   static int toto =15;



    public List<Book> filterBooksByAuthor(Author author, Book[] books);
    public List<Book> filterBooksByPublisher(Publisher publisher, Book[] books);

    public List<Book> filterBooksAfterSpecifiedYear(int yearFromInclusively, Book[] books);

    //Si Static => Interface utilisée sans implémenter de classe
//    public static List<Book> filterBooksAfterSpecifiedYear(int yearFromInclusively, Book[] books) {
//        return null;
//    }

    //A utiliser si implémenté dans plusieurs classes => peut être réécrite dans une ou pls classes, necessite un corps de méthode
//    default List<Book> filterBooksByAu(){
//
//    }
}
