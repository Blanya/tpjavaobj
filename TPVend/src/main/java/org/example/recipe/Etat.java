package org.example.recipe;

public enum Etat {
    ENTIER,
    CUIT,
    CRU,
    DECOUPE;
}
