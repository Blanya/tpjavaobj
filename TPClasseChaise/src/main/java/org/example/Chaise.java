package org.example;

public class Chaise {
    int nbPieds;
    String couleur, materiaux;

    public Chaise()
    {

    }

    public Chaise(int nbPieds, String couleur, String materiaux) {
        this.nbPieds = nbPieds;
        this.couleur = couleur;
        this.materiaux = materiaux;
    }

    @Override
    public String toString() {
        return "Je suis une Chaise, avec " + nbPieds +
                " pieds en " + materiaux + " de couleur " + couleur + '\'';
    }
}
