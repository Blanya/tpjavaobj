package org.example;

import java.util.Arrays;
import java.util.Scanner;

import static java.lang.String.valueOf;

public class LePendu {

    Scanner sc = new Scanner(System.in);
    int nbEssai;
    String motATrouver;
    String[] masque;

    private boolean testPresence;
    private String motTrouve;

    public LePendu(String motATrouver) {
        this.nbEssai = 10;
        this.motATrouver = motATrouver;
        this.masque = genererMasque(motATrouver);
    }

    public LePendu(int nbEssai, String motATrouver) {
        this.nbEssai = nbEssai;
        this.motATrouver = motATrouver;
        this.masque = genererMasque(motATrouver);
    }

    public int getNbEssai() {
        return nbEssai;
    }

    public void setNbEssai(int nbEssai) {
        this.nbEssai = nbEssai;
    }

    public String[] getMasque() {
        return masque;
    }

    public void setMasque(String[] masque) {
        this.masque = masque;
    }



    //generer motMasque
    protected String[] genererMasque(String motATrouver) {
        masque = new String[motATrouver.length()];

        for (int i = 0; i < motATrouver.length(); i++) {
            masque[i] = "* ";
        }
        System.out.println("Le mot à trouver est " + Arrays.toString(masque));
        testChar();
        return masque;
    }

    //tester les différents caractères
    protected void testChar() {

        System.out.println("Donne moi une lettre : ");;
        String letter = sc.next().toLowerCase();

        testPresence = motATrouver.contains(letter);

        if(!testPresence)
        {
            --nbEssai;
            System.out.println("Il vous reste " + nbEssai + " tentatives");
        }

        else
        {
            String[] proposition = new String[motATrouver.length()];
            if (motATrouver.contains(letter)) {
                for (int i = 0; i < motATrouver.length(); i++) {
                    if (letter.equals(valueOf(motATrouver.charAt(i)))) {
                        proposition[i] = letter;
                    } else {
                        proposition[i] = masque[i];
                    }
                }
            }
            masque = proposition;
        }

        motTrouve = String.join("", masque);

        if (nbEssai >= 1) {
            System.out.println(motTrouve);
        }
        testWin();
    }

    protected void testWin()
    {
        if (!motTrouve.equals(motATrouver)) {
            if (nbEssai == 0) {
                System.out.println("Dommage, vous avez perdu ! Le mot a trouver était " + motATrouver);
            } else {
                testChar();
            }
        }

        else
        {
            System.out.println("Bravo vous avez trouvé le mot caché ! ");
        }
    }

    @Override
    public String toString() {
        return "LePendu{" +
                "nbEssai=" + nbEssai +
                ", motATrouver='" + motATrouver + '\'' +
                ", masque=" + Arrays.toString(masque) +
                '}';
    }
}
