package org.example;

public class GenerateurDeMot {
    String[] mots = { "lavabo", "metro", "boulot", "bistrot", "gateau", "dodo"};

    private static int getRandom(int min, int max)
    {
        return (int) Math.floor(Math.random() * (max - min) + min);
    }

    protected String generer()
    {
        return mots[getRandom(0, mots.length-1)];
    }
}
