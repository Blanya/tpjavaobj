package org.example;

public abstract class Article {
    private static int count = 0;
    protected int id;

    public Article() {
        this.id = ++count;
    }

    public int getId() {
        return id;
    }
}
