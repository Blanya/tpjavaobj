package org.example;

public class Lot implements ArticleMap{
    public int refArticle;

    public ArticleUnitaire article;

    public int quantity;
    public int discount;

    public Lot(int refArticle, int quantity, int discount) {
        this.quantity = quantity;
        this.discount = discount;
        this.article = getArticle(refArticle);
    }

    public int getRefArticle() {
        return refArticle;
    }

    public ArticleUnitaire getArticle() {
        return article;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

}
