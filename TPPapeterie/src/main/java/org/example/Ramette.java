package org.example;

public class Ramette extends ArticleUnitaire implements ArticleMap{
    public int grammage;

    public Ramette(String name, double price, int grammage) {
        super(name, price);
        this.grammage = grammage;
        addArticle(this.id, this);
    }

    public int getGrammage() {
        return grammage;
    }

    public void setGrammage(int grammage) {
        this.grammage = grammage;
    }

    @Override
    public String toString() {
        return "Ramette{" +
                "ref= " + id +
                ", grammage=" + grammage +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
