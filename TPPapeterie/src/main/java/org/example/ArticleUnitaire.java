package org.example;

public abstract class ArticleUnitaire extends Article{
    public String name;
    public double price;

    public ArticleUnitaire(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
