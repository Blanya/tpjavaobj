package org.example;

import java.util.ArrayList;
import java.util.List;

public class Facture {
    public String client;
    private static int count = 0;
    private int number;
    public String date;
    private List<Ligne> purchases;
    private int nbPurchases;

    public Facture(String client, String date, int nbPurchases) {
        this.number = ++count;
        this.client = client;
        this.date = date;
        this.nbPurchases = nbPurchases;
        if(nbPurchases<10)
        {
            this.purchases = new ArrayList<>(nbPurchases);
        }
        else{
            this.purchases = new ArrayList<>(10);
        }
    }

    public String getClient() {
        return client;
    }

    public int getNumber() {
        return number;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Ligne> getPurchases() {
        return purchases;
    }

    public void setPurchases(List<Ligne> purchases) {
        this.purchases = purchases;
    }

    public int getNbPurchases() {
        return nbPurchases;
    }

    public void setNbPurchases(int nbPurchases) {
        this.nbPurchases = nbPurchases;
    }

    public void ajouterLigne(int refArticle, int qty){
        if(qty==1)
        {
            Ligne ligne = new Ligne(refArticle, qty);
            purchases.add(ligne);
        }
        else{
            int discount;
            if(qty<5)
                discount = 1;
            else if(qty<10)
                discount = 2;
            else if(qty<20)
                discount = 5;
            else
                discount = 8;

            Ligne ligne = new Ligne(new Lot(refArticle, qty, discount));
            purchases.add(ligne);
        }
    }

    public void getPrixTotal(){
        double sum = 0;
        ArticleUnitaire a;
        int qty;
        double price;
        double sumDiscount = 0;

        for (Ligne l: purchases) {
            try
            {
                a = l.getArticle();
                qty = l.getQuantity();
                sumDiscount += l.getPrice();
            }
            catch (NullPointerException e)
            {
                 a = l.getLot().getArticle();
                 qty = l.getQuantity();
                 sumDiscount += l.getPrice();
            }
            sum += a.getPrice() * qty;
        }
        System.out.println("Prix total non remisé " + sum) ;

        System.out.println("Prix total remisé " + sumDiscount);
    }

    @Override
    public String toString() {
        return "Facture n° " + number +
                " client: '" + client +
                ", date= " + date +
                " , achats=" + purchases;
    }
}
