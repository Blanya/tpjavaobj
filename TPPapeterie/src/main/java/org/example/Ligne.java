package org.example;

import org.w3c.dom.ls.LSOutput;

public class Ligne implements ArticleMap{
    private int refArticle;
    private int quantity;
    private ArticleUnitaire article;
    private Lot lot;

    private double price;

    public Ligne(int refArticle, int quantity) {
        this.article = getArticle(refArticle);
        this.quantity = quantity;
        this.price = article.getPrice();
    }

    public Ligne(Lot lot)
    {
        this.lot = lot;
        this.article = lot.getArticle();
        this.quantity = lot.getQuantity();
        this.price = article.getPrice() * (1 - lot.discount / 100.00 ) * this.quantity;
    }

    public int getRefArticle() {
        return refArticle;
    }

    public void setRefArticle(int refArticle) {
        this.refArticle = refArticle;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Lot getLot() {
        return lot;
    }

    public ArticleUnitaire getArticle() {
        return article;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String afficheLigne(){
        return  "\n Qty= " + quantity +
                ", " + article +
                ", PrixTotal = " + price ;
    }

    @Override
    public String toString() {
        return afficheLigne();
    }
}
