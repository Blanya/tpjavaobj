package org.example;

import java.util.HashMap;
import java.util.Map;

public interface ArticleMap {
    Map<Integer, ArticleUnitaire> map = new HashMap<>();

    default void addArticle(int key, ArticleUnitaire value){
        map.put(key, value);
    };

    default ArticleUnitaire getArticle(int ref){
       return map.get(ref);
    };
}
