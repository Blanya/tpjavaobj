package org.example;

public class Main {
    public static void main(String[] args) {
        Stylo stylo1 = new Stylo("Bic", 1.2, "blue");
        Stylo stylo2 = new Stylo("Bic", 1.2, "red");
        Stylo stylo3 = new Stylo("Bic", 1.2, "green");
        Stylo stylo4 = new Stylo("Bic", 1.2, "black");
        Stylo stylo5 = new Stylo("Bic", 2, "4 couleurs");

        Ramette ramette1 = new Ramette("Paper", 2.5, 200);
        Ramette ramette2 = new Ramette("PaperMate", 3.5, 200);
        Ramette ramette3 = new Ramette("Canson", 4.5, 350);
        Ramette ramette4 = new Ramette("Paper", 3.5, 250);

        Facture facture = new Facture("Michel Polna", "28/10/2022", 4);

        facture.ajouterLigne(2, 4);
        facture.ajouterLigne(1, 3);
        facture.ajouterLigne(5, 1);
        facture.ajouterLigne(8, 20);

        facture.getPrixTotal();
        System.out.println(facture);
    }
}