package org.example;

public class Stylo extends ArticleUnitaire implements ArticleMap {
    public String color;

    public Stylo(String name, double price, String color) {
        super(name, price);
        this.color = color;
        addArticle(this.id, this);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


    @Override
    public String toString() {
        return "Stylo{" +
                "ref = " + id +
                ", color='" + color + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
