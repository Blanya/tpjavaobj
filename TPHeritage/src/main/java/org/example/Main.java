package org.example;


import org.example.exercice2.Apartment;
import org.example.exercice2.Door;
import org.example.exercice2.Person;
import org.example.salarieCommerciale.Commerciale;
import org.example.salarieCommerciale.Salaries;
import org.example.tpSalaries.Salarie;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class Main {
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
//        TP STUDENT

//        Person person = new Person();
//        person.sayHello();
//
//        Student student = new Student(15);
//        student.sayHello();
//        student.GoToClasses();
//        student.displayAge();
//
//        Teacher teacher = new Teacher(40);
//        teacher.sayHello();
//        teacher.explain();

//        Apartment apt = new Apartment();
//        Person person = new Person("Thomas", apt);
//        person.display();

//        Salarie toto = new Salarie("Toto", 2000);
//        Salarie titi = new Salarie("Titi", 2300);
//        Salarie tata = new Salarie("Toto", 3000);
//        Salarie tete = new Salarie("Toto", 3500);
//
//        System.out.println("Le nombre de salarié(s) : " + Salarie.getCount());
//
//        List<Salarie> liste = new ArrayList<Salarie>();
//
//        liste.add(toto);
//        liste.add(titi);
//        liste.add(tata);
//        liste.add(tete);
//
//        int sum = 0;
//
//        for (Salarie s: liste ) {
//            s.afficherSalaire();
//            sum += s.getSalaire();
//        }
//
//        System.out.println("Le montant total des salaires est de " + sum);
//        Salarie.setCount(1);
//        System.out.println("Le nombre de salarié(s) : " + Salarie.getCount());

        Salaries salarie = new Salaries("Momo Data", 1500);
        Salaries salarie1 = new Salaries("MichMich Polo", 1800);
        Salaries salarie2 = new Salaries("Chantal Tor", 1200);
        Commerciale com = new Commerciale("Leslie Pola", 2000, 20000, 5);
        Commerciale com1 = new Commerciale("Brutus Tordu", 1200, 10200, 10);

        listeSalaries.add(salarie);
        listeSalaries.add(salarie1);
        listeSalaries.add(salarie2);
        listeSalaries.add(com1);
        listeSalaries.add(com);

        proposition();
    }

    public static void proposition(){

        System.out.println("==== Gestion des Employés ====");
        System.out.println();

        String[] propositions = {"1- Ajouter un employé", "2- Afficher le salaire des employés", "3- Rechercher un employé", "0- Quitter", "Entrez votre choix : "};

        for (String prop : propositions) {
            System.out.println(prop);
        }

        String answer = sc.next();

        switch (answer) {
            case "1" -> {
                menuEmploye();
                proposition();
            }
            case "2" -> {
                for (Salaries s : listeSalaries) {
                    System.out.println(s);
                    s.afficherSalaire();
                    //Pour la classe Commerciale
                    if(s instanceof Commerciale)
                    {
                        ((Commerciale) s).afficherSalaireCom();
                    }

                    System.out.println("------------------------");
                }
                proposition();
            }
            case "3" -> {
                System.out.println("==== Recherche Employé par Nom ====");
                System.out.println();
                System.out.println("Merci de saisir le nom : ");

                sc.nextLine();
                searchName = sc.nextLine();

                for (Salaries s : listeSalaries) {
                    if(Objects.equals(s.nom, searchName))
                    {
                        s.afficherSalaire();
                        if(s instanceof Commerciale)
                        {
                            ((Commerciale) s).afficherSalaireCom();
                        }
                    }
                }
                proposition();
            }
            case "0" -> {
                break;
            }
            default -> {
                System.out.println("Je n'ai pas compris votre demande ");
                proposition();
            }

        }
    }

    static String name;
    static int salaire;
    static  String searchName;
    static int commission;

    static int chiffreAffaire;

    static List<Salaries> listeSalaries = new ArrayList<>();
    public static void menuEmploye()
    {

        System.out.println("==== Ajouter un employé ====");
        System.out.println();

        String[] propositions = {"1-- Salarié", "2-- Commerciale", "0-- Retour", "Entrez votre choix : "};

        for (String prop : propositions) {
            System.out.println(prop);
        }

        String answer = sc.next();

        switch (answer) {
            case "1" -> {
                ajoutEmploye();
                Salaries salarie = new Salaries(name, salaire);
                listeSalaries.add(salarie);
                menuEmploye();
            }
            case "2" -> {
                ajoutCommerciale();
                Commerciale commerciale = new Commerciale(name, salaire, chiffreAffaire, commission);
                listeSalaries.add(commerciale);
                menuEmploye();
            }
            case "0" -> {
                break;
            }
            default -> {
                System.out.println("Je n'ai pas compris votre demande ");
                menuEmploye();
            }

        }
    }

    public static void ajoutEmploye()
    {
        System.out.println("Merci de saisir le nom");
        name = sc.nextLine();

        System.out.println("Merci de saisir le matricule");
        String matricule = sc.next();

        System.out.println("Merci de saisir la catégorie");
        String category = sc.next();

        System.out.println("Merci de saisir le service");
        String service = sc.next();

        System.out.println("Merci de saisir le salaire");
        salaire = parseInt(sc.next());
    }

    public static void ajoutCommerciale()
    {
        ajoutEmploye();
        System.out.println("Merci de saisir le chiffre d'affaire");
        chiffreAffaire = parseInt(sc.next());

        System.out.println("Merci de saisir la commission");
        commission = parseInt(sc.next());
    }
}