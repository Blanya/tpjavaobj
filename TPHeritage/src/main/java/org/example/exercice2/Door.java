package org.example.exercice2;

//composition with house (fait partie de, ne peut pas exister sans)
public class Door {
    protected String color;

    public Door(){
        color = "bleu";
    }

    public Door(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void display()
    {
        System.out.println("Je suis une porte, ma couleur est "+ color +".");
    }

}
